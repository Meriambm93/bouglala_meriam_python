import random
import sys

# Compléter les valeurs des variables
ENEMY_HEALTH = 50
PLAYER_HEALTH = 50
NUMBER_OF_POTIONS = 3
SKIP_TURN = False

while True:
    if SKIP_TURN:
        print("Vous passez votre tour...")
        SKIP_TURN = False
    else:
        user_choice = input("Souhaitez-vous attaquer (1) ou utiliser une potion (2) ? ")
        if user_choice not in ["1", "2"]:
            sys.exit()
        if user_choice == "1":  # Attaque
            your_attack = random.randint(5, 10)
            ENEMY_HEALTH -= your_attack
            print(f"Vous avez infligé {your_attack} points de dégats à l'ennemi ")
            print(f"Vous avez {PLAYER_HEALTH} points de vie")
            print(f"L'ennemi {ENEMY_HEALTH} points de vie")
        elif user_choice == "2" and NUMBER_OF_POTIONS > 0:
            potion_health = random.randint(15, 50)
            PLAYER_HEALTH += potion_health
            NUMBER_OF_POTIONS -= 1
            SKIP_TURN = True
            print(f"Vous récupérez {potion_health} points de vie ({NUMBER_OF_POTIONS} ? restantes)")
        else:
            print("Vous n'avez plus de potions...")
            continue

    enemy_attack = random.randint(5, 15)
    PLAYER_HEALTH -= enemy_attack
    print(f"L'ennemi vous a infligé {enemy_attack} points de dégats ")
    if PLAYER_HEALTH <= 0 or ENEMY_HEALTH <= 0:
        print(f"Vie du joueur {PLAYER_HEALTH} et celui de l'ennemi {ENEMY_HEALTH}")
        print("-" * 50)
        print("Fin du jeu.")
        sys.exit()


