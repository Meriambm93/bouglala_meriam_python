# Ecrire un programme en langage Python qui demande à l'utilisateur de saisir son nombre entier et de lui afficher si ce nombre est pair ou impair.
nombre = input("Tapez un nombre entier: ")
nombre = int(nombre)
if(nombre%2 == 0):
    print("Le nombre '", nombre, "'est pair ")
else:
    print("Le nombre '", nombre, "'est impair ")