# Ecrire un programme en Python qui demande à l'utilisateur 
# de saisir un nombre entier n et de lui afficher
#  la valeur de la somme 1 + 2 + … + n =

nombre = int(input("Tapez un nombre : "))
a = 0
for i in range(1,nombre+1):
    a = a + i 
print("La somme  1 + 2 + 3 + ...+ ",nombre," = : ", a)